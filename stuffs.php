<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no ,maximum-scale=1.0, user-scalable=no">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="I am Sanjaibal Dhanabal, I am a young system service engineer i can do the services with low cost" />
    <meta name="keywords" content="Sanjaibal Dhanabal, sanjay, web designer, os maintainance,mobile service ,hardware services" />
    <meta name="author" content="Praveenram Balachandran" />
    <title>Sanjaibal Dhanabal</title>
    <link rel="shortcut icon" href="img/logo.png">



    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="Sanjaibal Dhanabal (System Service Engineer)" />
    <meta property="og:image" content="http://sanjaibal.tk/img/whatsappshare.png" />
    <meta property="og:url" content="http://sanjaibal.tk" />
    <meta property="og:site_name" content="Sanjaibal Dhanabal" />
    <meta property="og:description" content="I am Sanjaibal Dhanabal, I am a young system service engineer i can do the services with low cost" />
    <meta name="twitter:title" content="Sanjaibal Dhanabal (System Service Engineer)" />
    <meta name="twitter:image" content="http://sanjaibal.tk/img/whatsappshare.png" />
    <meta name="twitter:url" content="http://sanjaibal.tk" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Radley" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/plugin.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />

    <script src="js/main.js"></script>
    <script src="js/pace.js"></script>
    <link rel="stylesheet" type="text/css" href="css/pace/pace-theme-flash.css">