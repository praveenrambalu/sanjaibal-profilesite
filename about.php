<!DOCTYPE html>
<html>

<head>
    <?php include ("stuffs.php"); ?>
</head>

<body>

    <!-- nav starts -->
    <div class="header" id="myHeader">
        <nav class="navbar " id="nav">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#"><img src="img/logo.png" class="topnav-logo"> Sanjaibal Dhanabal
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">
                            <a href="about.php">About Me</a>
                        </li>
                        <li>
                            <a href="services.php">Services</a>
                        </li>
                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>
    <!-- nav ends -->

    <!-- container nav -->
    <div class="container-fluid " id="wrapper ">
        <div class="section">
            <div class="row blue-grad ">
                <div class="text-center ">
                    <h1 class="text-white link-nav-heading ">
                        About Me
                    </h1>
                    <p class="text-white link-nav ">
                        <a href="index.php ">Home </a>
                        <span class="lnr lnr-arrow-right "></span>
                        <a href="about.php "> About Me</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- container nav ends -->
    <!-- about me -->
    <div class="container-fluid">
        <div class="row fs ">
            <div class="col-sm-4 wow fadeInLeft" data-wow-duration="2s">
                <div class="col-sm-1 "></div>
                <div class="col-sm-10 ">
                    <img src="img/1.jpg " class="img-responsive ">
                </div>
                <div class="col-sm-1 "></div>
            </div>


            <div class="col-sm-8 wow fadeInRight " data-wow-duration="2s">
                <div class="col-sm-1 "></div>
                <div class="col-sm-10 ">
                    <h6 class="banner-left-h6 ">ABOUT ME</h6>
                    <h1 class=" ">
                        <b>PERSONAL DETAILS</b>
                    </h1>
                    <p>I can do all the services like Desktop, Personal Computer (Laptop )and Mobile phones.</p>
                    <p> Also I can develop web pages with front and back end it includes Android Applications too.
                    </p>
                    <p>Here some of my details are listed based on my originality . i am sure that the given datas are real datas based on my knowledge</p>

                </div>
                <div class="col-sm-1 "></div>
            </div>

        </div>
        <hr>
    </div>
    <!-- about me ends -->
    <!-- skills starts -->
    <div class="container-fluid ">
        <div class="banner-title">
            <h6 class="banner-left-h6">My specialty</h6>
            <h2 class="in-exp-heading">My skills</h2>
        </div>



        <div class=" text-center row">


            <svg class="radial-progress wow slideInLeft" data-wow-duration="2s" data-percentage="90 " viewbox="0 0 80 80 ">
    <circle class="incomplete " cx="40 " cy="40 " r="35 "></circle>
    <circle class="complete " cx="40 " cy="40 " r="35 " style="stroke-dashoffset: 147.3406954533613; "></circle>
    <text class="percentage " x="50% " y="57% " transform="matrix(0, 1, -1, 0, 80, 0) ">OS</text>
</svg>




            <svg class="radial-progress wow slideInLeft" data-wow-duration="2s" data-percentage="75 " viewbox="0 0 80 80 ">
        <circle class="incomplete " cx="40 " cy="40 " r="35 "></circle>
        <circle class="complete " cx="40 " cy="40 " r="35 " style="stroke-dashoffset: 147.3406954533613; "></circle>
        <text class="percentage " x="50% " y="57% " transform="matrix(0, 1, -1, 0, 80, 0) ">Hardware</text>
         </svg>



            <svg class="radial-progress wow slideInUp" data-wow-duration="2s" data-percentage="60 " viewbox="0 0 80 80 ">
        <circle class="incomplete " cx="40 " cy="40 " r="35 "></circle>
        <circle class="complete " cx="40 " cy="40 " r="35 " style="stroke-dashoffset: 63.774330867872806; "></circle>
        <text class="percentage " x="50% " y="57% " transform="matrix(0, 1, -1, 0, 80, 0) ">Mobile</text>
    </svg>


            <svg class="radial-progress wow slideInRight" data-wow-duration="2s" data-percentage="50 " viewbox="0 0 80 80 ">
<circle class="incomplete " cx="40 " cy="40 " r="35 " ></circle>
        <circle class="complete " cx="40 " cy="40 " r="35 " style="stroke-dashoffset: 167.13272917097697; "  ></circle>
        <text class="percentage " x="50% " y="57% " transform="matrix(0, 1, -1, 0, 80, 0) ">Android</text>
    </svg>


            <svg class="radial-progress wow slideInRight" data-wow-duration="2s" data-percentage="50 " viewbox="0 0 80 80 ">
        <circle class="incomplete " cx="40 " cy="40 " r="35 "></circle>
        <circle class="complete " cx="40 " cy="40 " r="35 " style="stroke-dashoffset: 0; "></circle>
        <text class="percentage " x="50% " y="57% " transform="matrix(0, 1, -1, 0, 80, 0) ">Website</text>
    </svg>

        </div>
        <hr>
    </div>
    <!-- skills ends -->
    <!-- education starts -->
    <div class="container-fluid ">
        <div class="banner-title ">
            <h6 class="banner-left-h6 ">Education</h6>
            <h2 class="in-exp-heading ">Education Qualification</h2>


        </div>


        <div class="container text-center ">
            <table class="table table-hover ">
                <tbody>
                    <tr class="wow fadeInLeft" data-wow-duration="2s">
                        <td>
                            <b>B.E(CSE)</b>
                        </td>
                        <td>Mahendra Institute of Technology, Namakkal.</td>

                    </tr>
                    <tr class="wow fadeInRight" data-wow-duration="2s">
                        <td><b>HSC </b></td>
                        <td>Government Boys Higher Secondary School, Trichangode.</td>

                    </tr>
                    <tr class="wow fadeInLeft" data-wow-duration="2s">
                        <td><b>SSLC</b></td>
                        <td>MDV Higher Secondary School, Trichangode.</td>

                    </tr>
                </tbody>
            </table>
        </div>

    </div>
    <!-- edcuation ends -->




































    <br>
    <br>
    <br>

    <div class="container-fluid navbar-inverse navbar-bottom ">
        <?php include("footer.php"); ?>

    </div>



</body>
<script>
    window.onscroll = function() {
        myFunction()
    };

    var header = document.getElementById("myHeader");
    var nav = document.getElementById("nav");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
            nav.classList.add("color1");
        } else {
            header.classList.remove("sticky");
            nav.classList.remove("color1");
        }
    }
</script>
<script>
    $(document).ready(function() {
        $('.svg-progress-demo1').svgprogress({
            figure: "hexagon",
            progressFillGradient: ['#fcbf02', '#2cbc99'],
            progressWidth: 4
        });
    });
</script>

</html>