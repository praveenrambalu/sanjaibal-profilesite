<!DOCTYPE html>
<html>

<head>
    <?php include ("stuffs.php"); ?>
</head>

<body>

    <!-- nav starts -->
    <div class="header" id="myHeader">
        <nav class="navbar " id="nav">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#"><img src="img/logo.png" class="topnav-logo"> Sanjai
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="about.php">About Me</a>
                        </li>
                        <li class="active">
                            <a href="services.php">Services</a>
                        </li>
                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>
    <!-- nav ends -->

    <!-- container nav -->
    <div class="container-fluid " id="wrapper ">
        <div class="section">
            <div class="row blue-grad ">
                <div class="text-center ">
                    <h1 class="text-white link-nav-heading ">
                        Services
                    </h1>
                    <p class="text-white link-nav ">
                        <a href="index.php ">Home </a>
                        <span class="lnr lnr-arrow-right "></span>
                        <a href="services.php "> Services</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- container nav ends -->
    <div class="container-fluid">

        <div class="row fs">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div id="map">

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.27084915759!2d77.79942331480565!3d11.314902991957533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTHCsDE4JzUzLjciTiA3N8KwNDgnMDUuOCJF!5e0!3m2!1sen!2suk!4v1537071955725"
                        width="100%" height="300" frameborder="0" style="border:0" allowfullscreen="false"></iframe>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <hr>
        <div class="row fs">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <!-- <form id="contact-form"> -->
                <div class="col-sm-4">
                    <div id="address" class="wow fadeIn" data-wow-duration="2s">
                        <span class="lnr lnr-home title-icon"></span>
                        <span class="title">2/80 Ammasipalayam,Kollakaryanpettai(po)</span>
                        <br>
                        <span class="sub-title">Trichangode ,Namakkal 637007</span>
                    </div>
                    <div id="ad-phone" class="wow fadeIn" data-wow-duration="2s">
                        <span class="lnr lnr-phone title-icon"></span>
                        <span class="title">
                    <a href="tel:7373966701">+91 7373966701</a>
                    <a href="tel:8838887144">+91 8838887144</a>
                </span>
                        <br>
                        <span class="sub-title">Feel free to call me.</span>
                    </div>
                    <div id="mail" class="wow fadeIn" data-wow-duration="2s">
                        <span class="lnr lnr-envelope title-icon"></span>
                        <span class="title">
                    <a href="mailto:sanjaybal664@gmail.com">sanjaybal664@gmail.com</a>
                </span>
                        <br>
                        <span class="sub-title">Send me your Query anytime</span>
                    </div>
                </div>
                <form method="POST" action="mail.php">
                    <div class="col-sm-4 wow fadeInUp" data-wow-duration="2s">
                        <label for="name">Name:</label>
                        <span id="name-status"></span>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Sanjaybal" required autocomplete="off">

                        <label for="email">Email:</label>
                        <span id="email-status"></span>
                        <input type="email" class="form-control validate" name="email" id="email" placeholder="someone@example.com" required autocomplete="off">
                        <label for="phone">Phone (optional) :</label>
                        <input type="phone" class="form-control" name="phone" id="phone" placeholder="+91 98765XXXXX" autocomplete="off">

                    </div>
                    <div class="col-sm-4  wow fadeInRight" data-wow-duration="2s">
                        <label for="message">Message:</label>
                        <span id="message-status"></span>
                        <textarea type="text" class="form-control" name="message" id="message" autocomplete="off" placeholder="Type your message here.."></textarea>
                        <input type="submit" value="Send" name="submit" id="submit" class="btn btn-block blue-grad">
                    </div>
                </form>
                <!-- </form> -->
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>








































    <br>
    <br>
    <br>

    <div class="container-fluid navbar-inverse navbar-bottom ">
        <?php include("footer.php"); ?>

    </div>


</body>
<script>
    window.onscroll = function() {
        myFunction()
    };

    var header = document.getElementById("myHeader");
    var nav = document.getElementById("nav");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
            nav.classList.add("color1");
        } else {
            header.classList.remove("sticky");
            nav.classList.remove("color1");
        }
    }
</script>
<script>
    $("document").ready(function() {
        $(".buy1").hide();
        $(".buy2").hide();
        $(".buy3").hide();
        $(".buy4").hide();
        $(".card1").hover(function() {
            $(".buy1").toggle();
            $(".price1").toggle();
            $(".buy-btn").toggleClass("hoverclas");
            $(".pri-icon1").toggleClass("hovercircle");

        });
        $(".card2").hover(function() {
            $(".buy2").toggle();
            $(".price2").toggle();
            $(".buy-btn").toggleClass("hoverclas");
            $(".pri-icon2").toggleClass("hovercircle");


        });
        $(".card3").hover(function() {
            $(".buy3").toggle();
            $(".price3").toggle();
            $(".buy-btn").toggleClass("hoverclas");
            $(".pri-icon3").toggleClass("hovercircle");


        });


        $(".buy1").click(function() {
            window.open('contact.php', '_self')
        });
        $(".buy2").click(function() {
            window.open('contact.php', '_self')
        });
        $(".buy3").click(function() {
            window.open('contact.php', '_self')
        });

    });
</script>





</html>