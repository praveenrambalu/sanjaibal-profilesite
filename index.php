<!DOCTYPE html>
<html>

<head>

    <?php include ("stuffs.php"); ?>


</head>

<body>
    <div class="top-container">
        <div class="text-center">
            <img src="img/logo.png" class="logo">
        </div>
    </div>

    <div class="header" id="myHeader">
        <nav class="navbar " id="nav">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">Sanjaibal Dhanabal</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="about.php">About Me</a>
                        </li>
                        <li>
                            <a href="services.php">Services</a>
                        </li>
                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>



    <!-- first section wrapper -->
    <div class="container-fluid fs" id="wrapper" style="margin-top:100px">
        <div class="section  first-section fs">

            <div class="row " style="margin-top:20px;">
                <div class="col-sm-6 wow bounceInLeft" data-wow-duration="2s">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <h6 class="banner-left-h6">THIS IS ME</h6>
                        <h1 class="banner-left-h1">
                            <b>SANJAIBAL DHANABAL</b>
                        </h1>
                        <p>Hi.. This is <strong>Sanjaibal Dhanabal</strong>. Studying B.E. and working as a system service engineer well developed in hardware maintainance , mobile services and android app development.
                        </p>
                        <!-- <a href="img/praveenram_resume.pdf" class=" btn-download-cv btn-download-cv-a">Download CV<span class=" fa fa-sign-in rotate"></span></a> -->
                    </div>

                    <div class="col-sm-2"></div>
                </div>

                <div class="col-sm-6 wow bounceInRight" data-wow-duration="2s">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <img src="img/0.jpg" class="img-responsive">
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <hr>
        <br>
        <br>

        <!-- second section  -->
        <div class="section fs">
            <div class="row fs ">
                <div class="col-sm-6 wow bounceInLeft" data-wow-duration="2s">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <img src="img/1.jpg" class="img-responsive">
                    </div>
                    <div class="col-sm-1"></div>
                </div>


                <div class="col-sm-6 wow bounceInRight" data-wow-duration="2s">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <h6 class="banner-left-h6">ABOUT ME</h6>
                        <h1 class="banner-left-h1">
                            <b>PERSONAL DETAILS</b>
                        </h1>
                        <p>Desktops and personal computers (laptops ) are serviced by me with well accuracy and neat. It includes mobile services also.</p>
                        <!-- <a href="about.php" class=" btn-download-cv btn-download-cv-a">VIEW FULL DETAILS</a> -->
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>
        <hr>
    </div>


    <div class="container-fluid  fs">
        <div class="text-center fs">

            <h6 class="banner-left-h6 wow fadeInLeft" data-wow-duration="2s">what i do..?</h6>
            <h2 class="in-exp-heading wow fadeInRight" data-wow-duration="2s">Here are some of my expertise</h2>
        </div>


        <div class="row fs">
            <div class="col-sm-4">
                <div class="professional-box wow slideInLeft" data-wow-duration="2s">
                    <h2 class="d-flex align-items-center ">Desktop Service</h2>
                    <span class="box-logo fa fa-desktop font-color1" alt=" "></span>
                    <p>I can do all the service of desktop related problems like Display replacement, Mother Board Service, Hard disk Service,etc..,</p>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="professional-box wow slideInLeft" data-wow-duration="2s">
                    <h2 class="d-flex align-items-center ">Laptop Service</h2>
                    <span class="box-logo fa fa-laptop font-color2" alt=" "></span>
                    <p>I can do all the services related to laptop problems (Web Cam fitting, CD Drive Inserting, Extending RAM,ROM, etc..,)</p>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="professional-box wow slideInLeft" data-wow-duration="2s">
                    <h2 class="d-flex align-items-center ">OS maintainance</h2>
                    <span class="box-logo fas fa-compact-disc font-color3" alt=" "></span>
                    <p>I can solve all the problems that has in your computer including os installation and etc..,</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ">
            <div class="professional-box wow slideInRight" data-wow-duration="2s">
                <h2 class="d-flex align-items-center ">Mobile Service</h2>
                <span class="box-logo fas fa-mobile-alt font-color4" alt=" "></span>
                <p>I can solve all the problems related to Mobile Phone problems (Display, Charger Pin, Headset pin etc..,)</p>
            </div>
        </div>
        <div class="col-sm-4 ">
            <div class="professional-box wow slideInRight" data-wow-duration="2s">
                <h2 class="d-flex align-items-center ">Android App</h2>
                <span class="box-logo fab fa-android font-color5" alt=" "></span>
                <p>Android is a Growing Technology in this world .. I am creating app in it.. </p>
            </div>
        </div>
        <div class="col-sm-4 ">
            <div class="professional-box wow slideInRight" data-wow-duration="2s">
                <h2 class="d-flex align-items-center ">Web Development</h2>
                <span class="box-logo fa fa-code font-color6" alt=" "></span>
                <p>Websites are most important for marketing i am creating a professional and attractive websites </p>
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>
    <div class="container-fluid navbar-inverse navbar-bottom ">
      <?php include("footer.php"); ?>

    </div>





</body>
<script>
    window.onscroll = function() {
        myFunction()
    };

    var header = document.getElementById("myHeader");
    var nav = document.getElementById("nav");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
            nav.classList.add("color1");
        } else {
            header.classList.remove("sticky");
            nav.classList.remove("color1");
        }
    }
</script>

</html>